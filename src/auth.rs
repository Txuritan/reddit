use std::sync::{Arc, Mutex};

use base64::encode;

use hyper::{Body, Client, HeaderMap, Method, Request, Uri};
use hyper::client::HttpConnector;
use hyper::header::{self, HeaderValue};
use hyper::rt::{Future, Stream};
use hyper_tls::HttpsConnector;

use serde::de::DeserializeOwned;
use serde_json::de::from_slice;

use errors::Errors;
use responses::auth::{RevokeResponse, TokenResponse};

// Modified from the maplit crate
macro_rules! headermap {
    (@single $($x:tt)*) => (());
    (@count $($rest:expr),*) => (<[()]>::len(&[$(headermap!(@single $rest)),*]));

    ($($key:expr => $value:expr,)+) => { headermap!($($key => $value),+) };
    ($($key:expr => $value:expr),*) => {
        {
            let _cap = headermap!(@count $($key),*);
            let mut _map = HeaderMap::with_capacity(_cap);
            $(
                let _ = _map.insert($key, $value);
            )*
            _map
        }
    };
}

pub trait Auth {
    /// Logs in with given username and password/client id and secret.
    /// Will attempt to receive token and will fail if it doesn't receive one.
    fn login(
        &mut self,
        client: &Client<HttpsConnector<HttpConnector>, Body>,
        user_agent: &str,
    ) -> Result<(), Errors>;
    fn logout(
        &mut self,
        client: &Client<HttpsConnector<HttpConnector>, Body>,
        user_agent: &str,
    ) -> Result<(), Errors>;
}

/// Anonymous authenticator.
/// Does not use an account or client tokens.
/// Limited compared to PasswordAuth.
pub struct AnonymousAuth;

impl AnonymousAuth {
    pub fn new() -> Arc<Mutex<Box<Auth + Send>>> {
        Arc::new(Mutex::new(Box::new(AnonymousAuth {})))
    }
}

impl Auth for AnonymousAuth {
    fn login(
        &mut self,
        _client: &Client<HttpsConnector<HttpConnector>, Body>,
        _user_agent: &str,
    ) -> Result<(), Errors> {
        Ok(())
    }

    fn logout(
        &mut self,
        _client: &Client<HttpsConnector<HttpConnector>, Body>,
        _user_agent: &str,
    ) -> Result<(), Errors> {
        Ok(())
    }
}

/// OAuth based authenticator.
/// Uses username and password/client id and secret.
/// No limitations.
pub struct PasswordAuth {
    client_id: String,
    client_secret: String,
    token: Option<String>,
    user_name: String,
    user_password: String,
}

impl PasswordAuth {
    pub fn new(
        client_id: &str,
        client_secret: &str,
        user_name: &str,
        user_password: &str,
    ) -> Arc<Mutex<Box<Auth + Send>>> {
        Arc::new(Mutex::new(Box::new(PasswordAuth {
            client_id: client_id.to_string(),
            client_secret: client_secret.to_string(),
            token: None,
            user_name: user_name.to_string(),
            user_password: user_password.to_string(),
        })))
    }

    /// Hyper authorization header wrapper.
    /// Sets up the header string, encodes it, and wraps its in a HeaderValue.
    /// Probably not needed.
    fn build_auth_header(&self) -> HeaderValue {
        let header = format!("Basic {}:{}", self.client_id, self.client_secret);
        let header_encoded = encode(&header);
        HeaderValue::from_str(&header_encoded)
            .expect("Invalid header character, must be valid ASCII")
    }

    /// Hyper POST wrapper.
    /// Automatically sets the headers/body and preforms the request.
    fn hyper_post<T>(
        &self,
        client: &Client<HttpsConnector<HttpConnector>, Body>,
        uri: Uri,
        body: String,
        user_agent: &str,
    ) -> Box<Future<Item=T, Error=Errors>>
        where
            T: DeserializeOwned + 'static
    {
        let headers = headermap! {
            header::USER_AGENT => HeaderValue::from_str(user_agent)
                    .expect("Invalid header character, must be valid ASCII"),
            header::AUTHORIZATION => self.build_auth_header(),
        };

        let mut request = Request::new(Body::from(body));
        *request.method_mut() = Method::POST;
        *request.uri_mut() = uri;
        *request.headers_mut() = headers;

        let response = client
            .request(request)
            .map_err(Errors::from)
            .and_then(|res| {
                res
                    .body()
                    .concat2()
                    .map_err(Errors::from)
                    .and_then(move |body| {
                        if res.status().is_success() {
                            match from_slice::<T>(&body) {
                                Ok(data) => Ok(data),
                                Err(error) => Err(Errors::Json(error)),
                            }
                        } else {
                            Err(Errors::Http(res.status()))
                        }
                    })
            });

        Box::new(response)
    }
}

impl Auth for PasswordAuth {
    fn login(
        &mut self,
        client: &Client<HttpsConnector<HttpConnector>, Body>,
        user_agent: &str,
    ) -> Result<(), Errors> {
        let uri = "https://www.reddit.com/api/v1/access_token"
            .parse::<Uri>()
            .expect("Unable to parse URI, !!!THIS SHOULD NOT HAPPEN!!!");

        let body = format!(
            "grant_type=password&username={}&password={}",
            &self.user_name,
            &self.user_password,
        );

        let _res = self.hyper_post::<TokenResponse>(client, uri, body, user_agent);

        Ok(())
    }

    fn logout(
        &mut self,
        client: &Client<HttpsConnector<HttpConnector>, Body>,
        user_agent: &str,
    ) -> Result<(), Errors> {
        let uri = "https://www.reddit.com/api/v1/revoke_token"
            .parse::<Uri>()
            .expect("Unable to parse URI, !!!THIS SHOULD NOT HAPPEN!!!");

        let body = self.token.clone()
            .expect("Token missing, !!!THIS SHOULD NOT HAPPEN!!!");

        let _res = self.hyper_post::<RevokeResponse>(client, uri, body, user_agent);

        Ok(())
    }
}
