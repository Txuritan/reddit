use std::sync::{Arc, Mutex, MutexGuard};

use hyper::{Body, Client};
use hyper::client::HttpConnector;
use hyper_tls::HttpsConnector;

use auth::Auth;

pub struct RedditClient {
    auth: Arc<Mutex<Box<Auth + Send>>>,
    client: Client<HttpsConnector<HttpConnector>, Body>,
    user_agent: String,
}

impl RedditClient {
    pub fn new(
        auth: Arc<Mutex<Box<Auth + Send>>>,
        user_agent: &str,
    ) -> RedditClient {
        let https = HttpsConnector::new(2)
            .expect("Unable to create HTTPS connector");
        let client = Client::builder()
            .build::<HttpsConnector<HttpConnector>, Body>(https);

        let this = RedditClient {
            auth,
            client,
            user_agent: user_agent.to_string(),
        };

        this.get_auth()
            .login(&this.client, &this.user_agent)
            .expect("Authentication failed. Did you use the correct username/password?");

        this
    }

    pub fn get_auth(&self) -> MutexGuard<Box<Auth + Send + 'static>> {
        self.auth.lock().unwrap()
    }
}
