use std::convert::From;
use std::error::Error;
use std::fmt::{Display, Result, Formatter};

use hyper::{Error as HyperError, StatusCode};

use serde_json::{Error as JsonError};

#[derive(Debug)]
pub enum Errors {
    Http(StatusCode),
    Hyper(HyperError),
    Json(JsonError),
}

impl From<HyperError> for Errors {
    fn from(err: HyperError) -> Errors {
        Errors::Hyper(err)
    }
}

impl From<JsonError> for Errors {
    fn from(err: JsonError) -> Errors {
        Errors::Json(err)
    }
}

impl Display for Errors {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "Error! {}. ({:?})", self.description(), self)
    }
}

impl Error for Errors {
    fn description(&self) -> &str {
        match *self {
            Errors::Http(_) => "The API returned a non-success error code",
            Errors::Hyper(_) => "An error occurred while processing the HTTP response",
            _ => "General Error",
        }
    }
}
